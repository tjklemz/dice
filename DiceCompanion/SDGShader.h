//
//  SDGShader.h
//  DiceCompanion
//
//  Created by Company Account on 10/31/12.
//  Copyright (c) 2012 Sodogo LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SDGShader : NSObject
{
    GLuint hVS;
	GLuint hFS;
	GLuint hShaderProg;
	NSString * name;
}

-(id)initWithName:(NSString *)filename;

@end
