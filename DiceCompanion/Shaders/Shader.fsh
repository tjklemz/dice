//
//  Shader.fsh
//  DiceCompanion
//
//  Created by Company Account on 10/12/12.
//  Copyright (c) 2012 Sodogo LLC. All rights reserved.
//

varying lowp vec4 colorVarying;

void main()
{
    gl_FragColor = colorVarying;
}
