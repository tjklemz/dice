//
//  main.m
//  DiceCompanion
//
//  Created by Company Account on 10/12/12.
//  Copyright (c) 2012 Sodogo LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SDGAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SDGAppDelegate class]));
    }
}
