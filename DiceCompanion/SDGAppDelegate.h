//
//  SDGAppDelegate.h
//  DiceCompanion
//
//  Created by Company Account on 10/12/12.
//  Copyright (c) 2012 Sodogo LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SDGViewController;

@interface SDGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) SDGViewController *viewController;

@end
